#include "exampleSocketLib.hpp"

/* initialization starts the socket and inits class variables. 
 * It is my understanding epoll should be added here for I/O based interrupt handling of the socket.
 */
ExampleSocketLib::ExampleSocketLib(int port) {
  if ((objServer = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
      perror("Opening of Socket Failed !");
      exit(EXIT_FAILURE);
    }

  if ( setsockopt(objServer, SOL_SOCKET, SO_REUSEADDR, &opterr, sizeof(opterr))) {
      perror("Can't set the socket");
      exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);

    if (bind(objServer, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
      perror("Binding of socket failed !");
      exit(EXIT_FAILURE);
    }

    count = 0;
    incrCommand = "INCR";
    decrCommand = "DECR";
    outputCommand = "OUTPUT";
    openSocketsCount = 0;
}

ExampleSocketLib::~ExampleSocketLib() {

}

/* Checks the buffer for commands and returns a boolean that tells the caller whether 
 * all or just this connection should get a reply.
 */
bool ExampleSocketLib::executeRequest(std::string strBuffer) {
  if (strBuffer.find(incrCommand) != std::string::npos) {
    count++;
    printf("INCR command rec %d ! \n", count);
    return true;
  }

  else if (strBuffer.find(decrCommand) != std::string::npos) {
    count--;
    printf("DECR command rec %d ! \n", count);
    return true;
  }

  else if (strBuffer.find(outputCommand) != std::string::npos) {
    printf("OUTPUT command rec %d ! \n", count);
    return false;
  }
}

/* main execution of the program */
void ExampleSocketLib::start() {
  int addressLength = sizeof(address);
  int reader;
  int sock;
  int bufferLen = 1024;
  std::string message;

  while (true) {
    //Should be using epoll right now blocks after each new connection waiting for another
    char buffer[bufferLen] = {0}; 
    if (listen (objServer, 3) < 0) {
      perror ( "Can't listen from the server !");
      exit(EXIT_FAILURE);
    }

    if ((sock = accept(objServer, (struct sockaddr *)&address, (socklen_t*)&addressLength)) < 0) {
      perror("Accept");
      exit(EXIT_FAILURE);
    }

    //Not handling condition of more than 1024 connections
    //Need to loop back around and start closing old connections
    if (openSocketsCount != MAX_SOCKETS) {
      openSockets[openSocketsCount++] = sock;
    }
    reader = read(sock, buffer, bufferLen);
    std::string strBuffer(buffer, buffer + bufferLen);

    bool sendAll = executeRequest(strBuffer);
    message.clear();
    message = std::to_string(count);
    if(sendAll) {
      for (unsigned int i = 0; i < openSocketsCount; i++) {
        send(openSockets[i] , message.c_str(), message.length() , 0 );
      }
    }
    else {
      send(sock, message.c_str(), message.length() , 0 );
    }
  }
}