#ifndef EXAMPLE_SOCKET_LIB_HPP_
#define EXAMPLE_SOCKET_LIB_HPP_
#include <stdio.h>
#include <unistd.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string>

#define Default_Port 8089
#define MAX_SOCKETS 1024

class ExampleSocketLib {
public:
  ExampleSocketLib(int port=Default_Port);
  ~ExampleSocketLib(); 

  void start();
private:
  bool executeRequest(std::string strBuffer);

  int objServer;
  struct sockaddr_in address;
  int count;
  std::string incrCommand;
  std::string decrCommand;
  std::string outputCommand;
  int openSockets[MAX_SOCKETS];
  int openSocketsCount;
};

#endif